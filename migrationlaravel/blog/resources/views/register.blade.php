<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <header>
        <h1>Buat Account Baru!</h1>
    </header>
    <main>
        <h3>Sign Up Form</h3>
        <form action="welcome.blade.php">
            <label for="fname">First name:</label><br><br>
            <input type="text" id="fname" name="fname"><br><br>
            <label for="lname">Last name:</label><br><br>
            <input type="text" id="lname" name="lname"><br><br>

            <p>Gender:</p>
            <input type="radio" id="male" name="gender" value="male">
            <label for="male">Male</label><br>
            <input type="radio" id="female" name="gender" value="female">
            <label for="female">Female</label><br>
            <input type="radio" id="other" name="gender" value="other">
            <label for="other">Other</label>
            
            <br><br>
            <label for="nation">Nationality:</label>
            <select name="nation" id="nation">
              <option value="indonesia">Indonesia</option>
              <option value="malaysia">Malaysia</option>
            </select>
            
            <br>
            <p>language Spoken:</p>
            <input type="checkbox" id="language1" name="language1" value="indo">
            <label for="language1">Bahasa Indonesia</label><br>
            <input type="checkbox" id="language2" name="language2" value="english">
            <label for="language2">English</label><br>
            <input type="checkbox" id="language3" name="language3" value="other">
            <label for="language3">Other</label><br><br>

            <label for="bio">Bio:</label><br><br>
            <textarea id="bio" name="bio" rows="10" cols="50"></textarea><br><br>
            <input type="submit" id="submit" value="Sign Up">
        </form> 

          
        
    </main>    
</body>
</html>